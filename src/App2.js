import React from 'react'
import markupData from './dummyAuthenticationData/markup'
import Renderer from './Renderer';



console.log(markupData)
export default function App2() {
    return (
        <div>
           {markupData.map(config=> <Renderer config={config}></Renderer>)}
        </div>
    )
}
