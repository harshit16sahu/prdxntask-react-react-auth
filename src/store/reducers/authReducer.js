import ADD_USERS_IN_REDUX_STATE from '../actions/userActions'
import LOGIN_SUCCESS from '../actions/userActions'
import LOGIN_FAIL from '../actions/userActions'

const userData = require('../../dummyAuthenticationData/dummyData').default;
let user = JSON.parse(localStorage.getItem('user'));

const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
    switch (action.type) {
    case ADD_USERS_IN_REDUX_STATE: 
        return {
           userData : require('../../dummyAuthenticationData/dummyData').default,
           isLoggedIn : false
        }
    case 'LOGIN_SUCCESS' :
        return {
            ...state,
            isLoggedIn: true
        }

    case 'LOGIN_FAIL':
        return {
            ...state,
            isLoggedIn : false
        }
    case 'LOGOUT' : 
        return {
            ...state,
            isLoggedIn: false
        }
    default:
        return state
    }
}