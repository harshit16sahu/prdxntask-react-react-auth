const userActions = {    
    ADD_USERS_IN_REDUX_STATE : "ADD_USERS_IN_REDUX_STATE",
    LOGIN_SUCCESS : "LOGIN_SUCCESS",
    LOGIN_FAIL : "LOGIN_FAIL",
};


export default userActions;
