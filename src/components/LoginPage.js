import React, { Component } from 'react'
import {TextField, Container, Button} from '@material-ui/core';
import styled from 'styled-components';
import userAction from '../store/actions/userActions';
import {connect} from 'react-redux';
import _ from 'lodash';
import AlreadyLoggedInComponent from './AlreadyLoggedInComponent';
class LoginPage extends Component {
    state ={
        username: '',
        password: '',
        invalidUser: false,
        dataLoaded: false
    }

    handleChange = (e) =>{
        const { name, value } = e.target;
        this.setState({invalidUser: false})
        this.setState({ [name]: value });
    }

    handleSubmit =(e)=>{
        e.preventDefault();
        
        this.approveLogin(this.props.authentication.userData)
        this.setState({
            username: '',
            password: ''
        })

    }

    approveLogin=(data)=>{
        const username = data.filter(data1=> _.isEqual(this.state.username, data1.username))
        const password = data.filter(data2 => _.isEqual(this.state.password, data2.password))
        if(Object.keys(username).length === 0 || Object.keys(password).length === 0) this.setState({invalidUser: true})
        else this.props.dispatch({type: userAction.LOGIN_SUCCESS})
    }
    render() {
        if(this.props.authentication.isLoggedIn){
            return <AlreadyLoggedInComponent/>
        }
        return (
            <>
                <center>
                    <Container maxWidth="xs">
                        <FormStyling noValidate autoComplete="off">
                            <h1>Login Page</h1>
                            <TextField 
                                fullWidth 
                                id="filled-basic" 
                                label="Username" 
                                variant="filled" 
                                required
                                name = "username"
                                value = {this.state.username}
                                onChange={this.handleChange} />
                            <br/>
                            <br/>
                            <TextField 
                                fullWidth 
                                id="filled-basic" 
                                label="Password" 
                                variant="filled"  
                                required
                                name= "password"
                                value={this.state.password}
                                onChange={this.handleChange} />
                            <br/>
                            <br/><br/>
                            <br/>
                            <Button 
                                variant="contained" 
                                color="secondary"
                                type="submit"
                                onClick = {this.handleSubmit}
                                >
                                    Submit Form 
                            </Button>
                            <br/><br/><br/>
                        </FormStyling>
                    </Container>
                    {this.state.invalidUser && <span>Username or Password is Invalid, Please look at the state of the application to continue. </span>}
                </center>
            </>
        )
    }
}



function mapStateToProps(state){
    const {authentication} = state;
    return {
       authentication
    }
}

export default connect(mapStateToProps)(LoginPage);


const FormStyling = styled.form`
    background: #f3f3f3;
    padding: 20px;
    @media only screen and (max-width: 720px){
        margin-top: 5vh;
    }
`