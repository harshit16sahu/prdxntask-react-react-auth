import React, { Component } from 'react'
import { connect } from 'react-redux'
import App2 from '../App2'

class ProtectedPage extends Component {
    
    render() {
        if(this.props.authentication.isLoggedIn) {
            return (
                <>
                    <App2 />
                </>
            )
        }
        return (
            <center>
                <h1>You will have to be signed up to view this page</h1>
            </center>
        )
    }
}

const mapStateToProps = (state)=>{
    const {authentication} = state;
    return {
       authentication
    }
}
export default connect(mapStateToProps)(ProtectedPage);
