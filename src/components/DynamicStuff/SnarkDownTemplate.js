import React from 'react'
import snarkdown from 'snarkdown';

export default function SnarkDownTemplate(props) {
    let html = {__html: ''}
    if(props.properties.text != undefined) {
        let markdownTemplate = snarkdown(JSON.stringify(props.properties.text))
        html = {__html: markdownTemplate}
    }
    return (
        <center style={{background: 'red', color: 'white', padding: 10}}>
         {props.properties.text && <div dangerouslySetInnerHTML={html}></div>}
         {props.properties.children  && React.createElement(
             "div",
             props.properties.className,
             props.properties.children
         )}
        </center>
    )
}

