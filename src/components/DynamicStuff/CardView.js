import React from 'react'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


export default function CardView(props) {
    return (
        <div style={{ textAlign:'center', margin:'auto'}}>
            <Card variant="outlined" >
            <CardContent>
                <Typography  color="textSecondary" gutterBottom>
                    {props.obj.text}
                </Typography>
                <Typography variant="h5" component="h2">
                    {props.obj.title}
                </Typography>
              
              
            </CardContent>
            <CardActions>
                <Button fullWidth size="large" variant="outlined" color="secondary">Link {props.obj.link}</Button>
            </CardActions>
            </Card>
        </div>
    )
}
