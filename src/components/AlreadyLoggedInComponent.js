import React from 'react'
import { connect } from 'react-redux'
import {Button} from '@material-ui/core'

function AlreadyLoggedInComponent(props) {
    return (
        <>
            <center>
                <h1>You are Logged In</h1>
                <Button 
                    variant="contained"
                    color="secondary"
                    onClick={()=>{props.dispatch({type: 'LOGOUT'})}}
                >Click here to logout</Button>
            </center>
        </>
    )
}

const mapStateToProps=(state)=>{
    const {authentication} = state
    return{
        authentication
    }
}
export default connect(mapStateToProps)(AlreadyLoggedInComponent);