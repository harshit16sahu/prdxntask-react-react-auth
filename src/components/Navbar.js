import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const NavbarBackground =  styled.div`
    height: 12vh;
    background: #d3d3d3;
    display: flex;
    justify-content: space-between;
    padding: 0 200px;
    @media only screen and (max-width: 780px){
        padding: 0;
        justify-content: space-around;
        height: auto;
        text-align:center;
        line-height: 10px;
        
    }
`
const TextBlock = styled.h1`
        /* background: white; */
        padding: 5px;
    @media only screen and (max-width: 780px){
        font-size: 20px;
        display:flex;
    }
`
export default function Navbar() {
    return (
        <>
                <NavbarBackground>
                    <Link to="/"><TextBlock>HomePage</TextBlock></Link>
                    <Link to="/login"><TextBlock>Login</TextBlock></Link>
                    <Link to="/protected"><TextBlock>Protected</TextBlock></Link>
                </NavbarBackground>
        </>
    )
}
