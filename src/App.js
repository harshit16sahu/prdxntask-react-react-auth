import React from 'react';
import LoginPage from './components/LoginPage';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import Navbar from './components/Navbar';
import Homepage from './components/Homepage';
import ProtectedPage from './components/ProtectedPage';
import dummyData from './dummyAuthenticationData/dummyData';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ADD_USERS_IN_REDUX_STATE from './store/actions/userActions';
import { Grid } from '@material-ui/core';
import JSONPretty from 'react-json-pretty';
import 'react-json-pretty/themes/monikai.css';
import {Button} from '@material-ui/core'

class App extends React.Component {
  loadAllStateValues = () =>{
    this.props.dispatch({ 
      type: ADD_USERS_IN_REDUX_STATE,
    })
  }

  PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      this.props.state.authentication.isLoggedIn == true
        ? <Component {...props} />
        : <Redirect to='/login' />
    )} />
  )
  render(){
    return (
    <Router>
          <Navbar />
          <Grid container>
             <RedefineGrid item xl={7} justify="center" >
              <center>
              <h4>Use this data to login to the web application</h4>
              
              
              <Button  variant="contained" color="secondary" onClick={this.loadAllStateValues}>Load the Application State for feeding in All values.
              </Button>
                </center>
              <br/>
                <Switch>
                    <Route path ="/" component={Homepage} exact />
                    <Route path="/login" component={LoginPage} />
                    <this.PrivateRoute path="/protected" component= {ProtectedPage} />
                </Switch>
              </RedefineGrid>
            <RedefineGrid item xl={5}  style={{background:'', border:'2px solid #d3d3d3', padding: '30px'}} justify="center">
              <FooterForever>
                  The real time Application state is : 
                  <br/>
                  <p style={{width: 400}}>
                   {/* {JSON.stringify(this.props.state.authentication,null,2)} */}
                   <JSONPretty id="json-pretty" data={this.props.state.authentication}></JSONPretty>

                  </p>
              </FooterForever>
            </RedefineGrid>
           
          </Grid>
    </Router>

    );
  }
}
function mapStateToProps(state) {
  return {
    state
  };
}
const FooterForever = styled.div``
const RedefineGrid = styled(Grid)`
  width: 50%;
  @media only screen and (max-width: 780px){
    width: 100%;
  }
`
export default connect(mapStateToProps)(App);
