import React, { Component } from 'react'
import {Container, Card, Grid} from '@material-ui/core'
import styled  from 'styled-components';
import JSONPretty from 'react-json-pretty';
import 'react-json-pretty/themes/monikai.css';
import _ from 'lodash'
import CardView from './components/DynamicStuff/CardView';
import SnarkDownTemplate from './components/DynamicStuff/SnarkDownTemplate';

const Column = styled.div``
const Row = styled.div``
const KeyMapper = {
    Section : Container,
    Col : Column,
    Card: Card,
    Row: Row,

}

const Renderer = config => {
    const configuration = config.config;
    
    if(typeof KeyMapper[configuration.component] !== "undefined"){
        return React.createElement(
                "div", {},
                (configuration.props && configuration.props.children) ? 
                    configuration.props.children.map(data=> <Renderer config={data}/>) :
            //  <h1>HELLO {JSON.stringify(configuration.props)}</h1>
                <CardView obj={configuration.props} />
            )
           }
    else return <SnarkDownTemplate properties={configuration.props} />
  };


export default Renderer;