const data = [
    {
        component: 'Section',
        props: {
            h2: 'Section 1 Header',
            text: 'Section 1 summary text',
            children: [
                {
                    component: 'Col',
                    props: {
                        className: 'col',
                        children: [
                            {
                                component: 'Card',
                                props: {
                                    title: 'Blog 1',
                                    link: '/blog-1/',
                                    text:
                                        'Blog number 1',
                                },
                            },
                        ],
                    },
                },
                {
                    component: 'Col',
                    props: {
                        className: 'col',
                        children: [
                            {
                                component: 'Card',
                                props: {
                                    title: 'Blog 2',
                                    link: '/blog-2/',
                                    text:
                                        'Blog number 2',
                                },
                            },
                        ],
                    },
                },
                {
                    component: 'Col',
                    props: {
                        className: 'col',
                        children: [
                            {
                                component: 'Card',
                                props: {
                                    title: 'Blog 3',
                                    link: '/blog-3/',
                                    text:
                                        'Blog number 3',
                                },
                            },
                        ],
                    },
                },
            ],
        },
    },
    {
        component: 'Row',
        props: {
            children: [
                {
                    component: 'Col',
                    props: {
                        className: 'col',
                        children: [
                            {
                                component: 'Markdown',
                                props: {
                                    text: `## Features
                                    - **Fast:** since it's basically one regex and a huge if statement
                                    - **Tiny:** it's 1kb of gzipped ES3
                                    - **Simple:** pass a Markdown string, get back an HTML string `,
                                },
                            },
                        ],
                    },
                },
                {
                    component: 'Col',
                    props: {
                        className: 'col-6',
                        children: [
                            {
                                component: 'Row',
                                props: {
                                    children: [
                                        {
                                            component: 'Col',
                                            props: {
                                                children: [
                                                    {
                                                        component: 'Tag',
                                                        props: {
                                                            children:
                                                                'I am sub sub child',
                                                            className:
                                                                'sub sub text',
                                                            tag: 'div',
                                                        },
                                                    },
                                                ],
                                            },
                                        },
                                    ],
                                },
                            },
                        ],
                    },
                },
                {
                    component: 'Col',
                    props: {
                        children: [
                            {
                                component: 'Tag',
                                props: {
                                    children: 'I am sub sub child',
                                    className: 'sub sub text',
                                    tag: 'div',
                                },
                            },
                        ],
                    },
                },
            ],
        },
    },
    {
        component: 'Row',
        props: {
            children: [
                {
                    component: 'Col',
                    props: {
                        className: 'col',
                        children: [
                            {
                                component: 'Card',
                                props: {
                                    title: 'Blogs to be worked upon',
                                    link: 'Follow us on Facebook and Twitter',
                                    text:
                                        'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.',
                                },
                            },
                        ],
                    },
                },
                {
                    component: 'Col',
                    props: {
                        className: 'col',
                        children: [
                            {
                                component: 'Card',
                                props: {
                                    title: 'Blog',
                                    link: '/blog/',
                                    text:
                                        'Get updates and advice on senior healthcare straight from our blog',
                                },
                            },
                        ],
                    },
                },
                {
                    component: 'Col',
                    props: {
                        className: 'col',
                        children: [
                            {
                                component: 'Card',
                                props: {
                                    title: 'Blog',
                                    link: '/blog/',
                                    text:
                                        'Get updates and advice on senior healthcare straight from our blog',
                                },
                            },
                        ],
                    },
                },
            ],
        },
    },
]; 

export default data