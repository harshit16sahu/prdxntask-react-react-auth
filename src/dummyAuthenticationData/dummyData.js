import dummyBackend from "./dummyBackend"

const dummyData = [
    {
        id: 1,
        username: 'harshit',
        emailID: 'harshit16sahu@gmail.com',
        password: 'Harshit@123'
    },
    {
        id: 2,
        username: 'priyanshu',
        emailID: 'priyanshu@gmail.com',
        password: 'Priyanshu@123'
    }
]
export default dummyData;
